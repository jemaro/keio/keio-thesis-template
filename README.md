# KEIO Thesis Template

This template follows the guidelines specified in the [Course
Guidebook](https://www.students.keio.ac.jp/en/yg/gsst/class/files/Course_Guidebook.pdf)
(pages 27 and 28).